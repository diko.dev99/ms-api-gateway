// file index for load all data in folder handler/media

const create = require('./create')
const getAll = require('./getAll')
const destroy = require('./destroy')

module.exports = {
    getAll,
    create,
    destroy
}