const express = require('express');
const router = express.Router();
const handlerAdapter = require('./handler/media')

router.get('/', handlerAdapter.getAll);
router.post('/', handlerAdapter.create);
router.delete('/:id', handlerAdapter.destroy);

module.exports = router;
