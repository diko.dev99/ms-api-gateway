const express = require('express');
const router = express.Router();
const { APP_NAME } = process.env //import variable env from file .env

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('payments'); //show response
});

module.exports = router;
